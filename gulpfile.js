var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    fs = require('fs'),
    browserify = require('browserify'),
    watchify = require('watchify'),
    vueify = require('vueify'),
    exorcist = require('exorcist'),
    envify = require('envify'),
    eslint = require('gulp-eslint'),
    connect = require('gulp-connect-php'),
    mkdirp = require('mkdirp'),
    del = require('del'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    size = require('gulp-size'),
    historyFallback = require('connect-history-api-fallback'),
    paths = {
        entryfile: 'main.js',
        bundlename: 'bundle',
        src: 'src',
        public: 'public',
        test: 'test',
        app: 'public/app'
    },
    s3 = require('gulp-s3-deploy');

/* Get params passed to gulp */
const args = (argList => {
    let arg = {}, a, opt, thisOpt, curOpt;
    for (a = 0; a < argList.length; a++) {
        thisOpt = argList[a].trim();
        opt = thisOpt.replace(/^\-+/, '');
        if (opt === thisOpt) {
        if (curOpt) arg[curOpt] = opt;
        curOpt = null;
        }
        else {
        curOpt = opt;
        arg[curOpt] = true;
        }
    }
    return arg;
})(process.argv);

/* Move these to .env later */
var s3Credentials = {
    "key":    "AKIAJFFK5Y4SEQVYDEYQ",
    "secret": "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",
    "bucket": "develop.renty.co.nz",
    "region": "ap-southeast-2"
};

// Deploy to S3
gulp.task('deployS3', function () {
    /* If bucket is passed through command use it.  eg. gulp deployS3 --bucket "develop.renty.co.nz" */
    if(args.bucket){
        s3Credentials.bucket = args.bucket;
    }
    console.log("Deploying to S3... bucket : "+args.bucket);
    gulp.src( './public/**' )
    .pipe( s3( s3Credentials ) );
});

// run php server (not used)
gulp.task('serve:php', function () {
    connect.server({}, function () {
        var bs = browserSync.init({
            proxy: 'localhost:8000',
            https: true,
            notify: false,
            reloadDebounce: 1000
        });
        /* Watch the src for changes, then re-lint and re-bundle */
        gulp.watch(paths.src + "/*.js").on('change', function () { gulp.run(['lint', '_bundle']); });
        /* Watch the app for changes, then reload browswer */
        gulp.watch(paths.app + "/*.js").on('change', browserSync.reload);
    });
});

// run server
gulp.task('_server', function () {
    var bs = browserSync.init({
        server: {
            baseDir: "./public",
            middleware: [ historyFallback() ]
        },
        https: true,
        notify: false,
        reloadDebounce: 1000
    });
    /* Watch the src for changes, then re-lint and re-bundle */
    gulp.watch(paths.src + "/**/*.*").on('change', function () { gulp.run(['lint', '_bundle']); });
    /* Watch the app for changes, then reload browswer */
    gulp.watch(paths.app + "/**/*.*").on('change', browserSync.reload);
});

// bundle for serve
gulp.task('_bundle', function () {

    browserify(paths.src + '/' + paths.entryfile, { debug: true })
        .plugin('vueify/plugins/extract-css', {
            out: paths.app + '/' + paths.bundlename + '.css'
        })
        .bundle()
        .pipe(fs.createWriteStream(paths.app + '/' + paths.bundlename + '.js'));
});

// bundle for build
gulp.task('_bundle_build', function () {
    browserify(paths.src + '/' + paths.entryfile, { debug: true })
        .plugin('vueify/plugins/extract-css', {
            out: paths.app + '/' + paths.bundlename + '.css'
        })
        .bundle()
        .pipe(envify())
        .pipe(source(paths.bundlename + '.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(size())
        .pipe(gulp.dest(paths.app));
});

/* Public gulp tasks */
// lint
gulp.task('lint', function () {
    gulp.src(paths.src + '/**/*.js')
        .pipe(eslint())
        .pipe(eslint.format());
});

gulp.task('prep-build', function () {
    // create necessary files and folder to build the application
    if (!fs.existsSync(paths.public)) {
        fs.mkdirSync(paths.public);
    }
    del(paths.public + '/*.*');
    if (!fs.existsSync(paths.app)) {
        fs.mkdirSync(paths.app);
    }

    /* Build files */
    gulp.src('./index.html').pipe(gulp.dest('./public/'));
});

/* Set environment to production */
gulp.task('set-env-prod', function() {
    return process.env.NODE_ENV = 'production';
});

// serve.  Retain source maps, larger file sizes
gulp.task('serve', ['lint', 'prep-build', '_bundle', '_server']);

// build.  No source maps, compress files for deployment
gulp.task('build', ['set-env-prod', 'prep-build', '_bundle_build']);
