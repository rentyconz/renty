const config = {
  // firebase configurations
  firebase: {
    apiKey: 'AIzaSyA9nr5um25ko0hPRdnPdJzTyRvFf86EzyM',
    authDomain: 'renty-server.firebaseapp.com',
    databaseURL: 'https://renty-server.firebaseio.com',
    projectId: 'renty-server',
    storageBucket: 'renty-server.appspot.com',
    messagingSenderId: '547404466233'
  },
  google: {
    mapKey: 'AIzaSyB7zHUG1r6f8xNjOG3qGke2CgzBSzrg3fo'
  }
}

export default config
