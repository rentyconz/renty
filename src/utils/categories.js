export function setUpCategoriesForDisplay (categories) {
  const categoriesMap = []
  // collecting header
  for (var i = 0; i < categories.length; i++) {
    var category = categories[i]
    if (category.parentId === null) {
      const categoryHead = { header: category.name }
      categoriesMap.push(categoryHead)
      const divider = { divider: true }
      categoriesMap.push(divider)
      for (var j = 0; j < categories.length; j++) {
        var subcategory = categories[j]
        if (
          subcategory.parentId !== null &&
          subcategory.parentId === category.id
        ) {
          subcategory.parentName = category.name
          categoriesMap.push(subcategory)
        }
      }
    }
  }
  return categoriesMap
}

export function getParentId (categoryId, categories) {
  for (var i = 0; i < categories.length; i++) {
    if (categories[i].id === categoryId) {
      return categories[i].parentId
    }
  }
  return null
}
