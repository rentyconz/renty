import config from './../config'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

const firebaseApp = firebase.initializeApp(config.firebase)

export default firebaseApp.firestore()
