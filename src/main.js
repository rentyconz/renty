import Vue from 'vue'
import Vuetify from 'vuetify'
import VueFirestore from 'vue-firestore'
import router from './router'
import store from './store'
import App from './MainApp.vue'
import AlertCmp from './components/shared/Alert.vue'
import LoadingCmp from './components/shared/Loading.vue'
import firebase from 'firebase/app'
import config from './config'
import * as VueGoogleMaps from 'vue2-google-maps'
import 'firebase/auth' /* Load in just what we need */
import './db'

Vue.component('error-alert', AlertCmp)
Vue.component('loading-alert', LoadingCmp)

Vue.use(Vuetify)
Vue.use(VueFirestore)
Vue.use(VueGoogleMaps, {
  load: {
    key: config.google.mapKey,
    libraries: 'places'
  }
})

// eslint-disable-line
new Vue({
  el: '#root',
  router,
  store,
  render: (h) => h(App),
  created () {
    store.commit('setLoading', { action: 'Initializing app' })
    store.dispatch('fetchListOfCategories')
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        store.commit('setAuth', user)
        store.dispatch('fetchUserData', user.uid)
        // move these to the un authenticate
        store.dispatch('fetchListOfCategories')
      } else {
        store.commit('setAuth', null)
        store.commit('setUser', null)
      }
      store.commit('setLoading', false)
    })
  }
})
