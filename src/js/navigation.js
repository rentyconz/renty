export function getNavigationItems (isLoggedIn) {
  var items = [{
    href: 'home',
    router: true,
    title: 'Home',
    icon: 'home'
  }]

  var loggedOutItems = [{
    href: 'login',
    router: true,
    title: 'Sign In',
    icon: 'domain'
  }, {
    href: 'signup',
    router: true,
    title: 'Sign Up',
    icon: 'domain'
  }]

  var loggedInItems = [{
    href: 'profile',
    router: true,
    title: 'Profile',
    icon: 'domain'
  }, {
    href: 'dashboard',
    router: true,
    title: 'Dashboard',
    icon: 'domain'
  }, {
    href: 'item',
    router: true,
    title: 'Item',
    icon: 'domain'
  }]

  if (isLoggedIn) {
    for (var i = 0; i < loggedInItems.length; i++) {
      items.push(loggedInItems[i])
    }
  } else {
    for (var j = 0; j < loggedOutItems.length; j++) {
      items.push(loggedOutItems[j])
    }
  }

  return items
}
