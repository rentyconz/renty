import store from '../store'

export default (to, from, next) => {
  const currentUser = store.getters.auth
  if (currentUser !== null) {
    next()
  } else {
    next('/login')
  }
}
