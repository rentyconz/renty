
import Vue from 'vue'
import VueRouter from 'vue-router'
import NotFound from './../components/NotFound.vue'
import AuthGuard from './auth-guard'
// empty page
import Model from './../components/model/model.vue'
// auth
import Login from './../components/Login.vue'
import ForgotPassword from './../components/ForgotPassword.vue'
import Signup from './../components/Signup.vue'
// homepage
import Home from './../components/Home.vue'
// me
import Dashboard from './../components/Dashboard.vue'
// me/profile
import UpdateProfile from './../components/profile/Update.vue'
import ViewProfile from './../components/profile/View.vue'
// me/listing
import ListItems from './../components/listing/List.vue'
import CreateItem from './../components/listing/Create.vue'
import UpdateItem from './../components/listing/Update.vue'
import ViewItem from './../components/listing/View.vue'
// item view by customer
import CustomerListItems from './../components/item/List.vue'
import CustomerViewItem from './../components/item/View.vue'

/* Use Router */
Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', name: 'home', component: Home },
    { path: '/login', name: 'login', component: Login },
    { path: '/forgotpassword', name: 'forgotpassword', component: ForgotPassword },
    { path: '/signup', name: 'signup', component: Signup },
    {
      path: '/me', component: Model, beforeEnter: AuthGuard,
      children: [
        { path: '', name: 'dashboard', component: Dashboard },
        {
          path: 'profile', component: Model,
          children: [
            { path: '', name: 'profile', component: ViewProfile },
            { path: 'update', name: 'updateProfile', component: UpdateProfile }
          ]
        },
        {
          path: 'listing', component: Model,
          children: [
            { path: '', name: 'listing', component: ListItems },
            { path: 'create', name: 'createItem', component: CreateItem },
            {
              path: ':id', component: Model,
              children: [
                { path: '', name: 'viewItem', component: ViewItem, props: true },
                { path: 'update', name: 'updateItem', component: UpdateItem, props: true }
              ]
            }
          ]
        }
      ]
    },
    {
      path: '/items', component: Model, beforeEnter: AuthGuard,
      children: [
        { path: '', name: 'allItems', component: CustomerListItems },
        {
          path: ':category', component: Model,
          children: [
            { path: '', name: 'category', component: CustomerListItems, props: true },
            {
              path: ':subcategory', component: Model,
              children: [
                { path: '', name: 'subcategory', component: CustomerListItems, props: true },
                {
                  path: ':id', component: Model,
                  children: [
                    { path: '', name: 'viewItemFromPath', component: CustomerViewItem, props: true }
                  ]
                }
              ]
            }
          ]
        }
      ]
    },
    { path: '*', name: 'page not found', component: NotFound }
  ]
})

export default router
