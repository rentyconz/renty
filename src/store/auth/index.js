import firebase from 'firebase/app'
import 'firebase/auth'
import router from '../../router'

export default {
  state: {
    auth: {
      uid: null,
      photoUrl: null,
      displayName: null,
      email: null
    }
  },
  mutations: {
    setAuth (state, payload) {
      state.auth = payload
    }
  },
  actions: {
    signUp ({ commit }, payload) {
      // using firebase to sign up with email
      commit('setLoading', { action: 'Signing Up' })
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then((user) => {
          commit('setAuth', user)
          commit('setLoading', false)
          // move to home
          router.push('/')
        })
        .catch(e => {
          commit('setError', e)
          commit('setLoading', false)
        })
    },
    signOut ({ commit }) {
      commit('setLoading', { action: 'Signing Out' })
      firebase.auth().signOut()
        .then(() => {
          commit('setAuth', null)
          commit('setUser', null)
          commit('setLoading', false)
          // move to login
          router.push('/login')
        })
        .catch(e => {
          commit('setError', e)
          commit('setLoading', false)
        })
    },
    signIn ({ commit }, payload) {
      // using firebase to sign in with email verification
      commit('setLoading', { action: 'Signing In' })
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        .then((user) => {
          commit('setAuth', user)
          commit('setLoading', false)
          // move to home
          router.push('/')
        })
        .catch(e => {
          commit('setError', e)
          commit('setLoading', false)
        })
    },
    signInViaGoogle ({ commit }) {
      // using firebase to sign in with google verification
      commit('setLoading', { action: 'Signing In Via Google' })
      const provider = new firebase.auth.GoogleAuthProvider()
      firebase.auth().signInWithPopup(provider)
        .then((user) => {
          commit('setAuth', user)
          commit('setLoading', false)
          // move to home
          router.push('/')
        })
        .catch(e => {
          commit('setError', e)
          commit('setLoading', false)
        })
    },
    sendEmailVerification ({ getters }) {
      const user = getters.auth
      user.sendEmailVerification()
        .then(function () {
          window.alert('Email Verification Sent!')
        })
        .catch(e => window.alert(e.code + ' - ' + e.message))
    },
    sendPasswordReset ({ commit }, email) {
      commit('setLoading', { action: 'Sending Password Reset' })
      firebase.auth().sendPasswordResetEmail(email)
        .then(function () {
          window.alert('Password Reset Email Sent!')
          commit('setLoading', false)
        })
        .catch(e => {
          commit('setError', e)
          commit('setLoading', false)
        })
    }
  },
  getters: {
    auth (state) {
      return state.auth
    }
  }
}
