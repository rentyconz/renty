import firebase from 'firebase/app'
import 'firebase/storage'
import 'firebase/auth'
import db from '../../db'
import router from '../../router'

export default {
  state: {
    items: [],
    item: null
  },
  mutations: {
    setItems (state, payload) {
      state.items = payload
    },
    setItem (state, payload) {
      state.item = payload
    }
  },
  actions: {
    fetchListOfItems ({ commit, getters }, payload) {
      // payload and getters use for future querying and filtering
      commit('setLoading', { action: 'Fetching Items' })
      let query = db.collection('items')
      if (payload) {
        if (payload.userId) {
          query = query.where('creatorId', '==', payload.userId)
        }
        if (payload.parentCategoryId) {
          query = query.where('parentCategoryId', '==', payload.parentCategoryId)
        }
        if (payload.categoryId) {
          query = query.where('categoryId', '==', payload.categoryId)
        }
      }

      query
        .get()
        .then(data => {
          const items = []
          data.forEach(function (item) {
            const itemData = item.data()
            itemData.id = item.id
            items.push(itemData)
          })
          commit('setItems', items)
          commit('setLoading', false)
        })
        .catch(e => {
          commit('setError', e)
          commit('setLoading', false)
        })
    },
    fetchAnItem ({ commit, getters }, id) {
      // payload and getters use for future querying and filtering
      commit('setLoading', { action: 'Fetching Item' })
      db.doc('items/' + id)
        .get()
        .then(data => {
          if (!data.exists) {
            commit('setItem', null)
            commit('setLoading', false)
            router.push('/*')
          } else {
            const itemData = data.data()
            itemData.id = data.id
            commit('setItem', itemData)
            commit('setLoading', false)
          }
        })
        .catch(e => {
          commit('setError', e)
          commit('setLoading', false)
        })
    },
    createItem ({ commit, getters }, payload) {
      commit('setLoading', { action: 'Create Listing' })

      let imageUrl
      let itemId
      const creatorId = getters.auth.uid
      const userInfo = getters.user
      const itemData = {
        title: payload.title,
        price: payload.price,
        description: payload.description,
        location: userInfo.address,
        createdData: new Date().toLocaleString(),
        updatedData: new Date().toLocaleString(),
        parentCategoryId: payload.parentCategoryId,
        categoryId: payload.categoryId,
        creatorId: creatorId
      }

      db.collection('items').add(itemData)
        .then((data) => {
          itemId = data.id
          return itemId
        })
        .then(function () {
          if (payload.image) {
            const filename = payload.image.name
            const ext = filename.slice(filename.lastIndexOf('.'))
            return firebase.storage().ref('images/items/' + itemId + '.' + ext).put(payload.image)
          } else {
            return null
          }
        })
        .then(fileData => {
          if (fileData) {
            imageUrl = fileData.metadata.downloadURLs[0]
          } else {
            imageUrl = 'https://pngimg.com/uploads/minions/minions_PNG24.png'
          }
          return db.doc('items/' + itemId).update({ imageUrl: imageUrl })
        })
        .then(() => {
          commit('setLoading', false)
          this.dispatch('fetchListOfItems', { userId: creatorId })
          router.push('/me/listing')
        })
        .catch(e => {
          commit('setError', e)
          commit('setLoading', false)
        })
    },
    updateItem ({ commit, getters }, payload) {
      commit('setLoading', { action: 'Update Listing' })

      let imageUrl
      const itemId = payload.id
      const creatorId = getters.auth.uid
      const userInfo = getters.user
      const itemData = {
        title: payload.title,
        price: payload.price,
        description: payload.description,
        location: userInfo.address,
        createdData: new Date().toLocaleString(),
        updatedData: new Date().toLocaleString(),
        parentCategoryId: payload.parentCategoryId,
        categoryId: payload.categoryId,
        creatorId: creatorId
      }

      db.doc('items/' + itemId).update(itemData)
        .then(function () {
          if (payload.image) {
            const filename = payload.image.name
            const ext = filename.slice(filename.lastIndexOf('.'))
            return firebase.storage().ref('images/items/' + itemId + '.' + ext).put(payload.image)
          } else {
            return null
          }
        })
        .then(fileData => {
          if (fileData) {
            imageUrl = fileData.metadata.downloadURLs[0]
          } else {
            if (payload.imageUrl) {
              imageUrl = payload.imageUrl
            } else {
              imageUrl = 'https://pngimg.com/uploads/minions/minions_PNG24.png'
            }
          }
          return db.doc('items/' + itemId).update({ imageUrl: imageUrl })
        })
        .then(() => {
          commit('setLoading', false)
          this.dispatch('fetchListOfItems', { userId: creatorId })
          router.push('/me/listing/' + itemId)
        })
        .catch(e => {
          commit('setError', e)
          commit('setLoading', false)
        })
    }
  },
  getters: {
    items (state) {
      return state.items
    },
    item (state) {
      return state.item
    },
    itemImgs (state) {
      return state.item.imageUrl
    }
  }
}
