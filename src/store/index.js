import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
import user from './user'
import shared from './shared'
import item from './item'
import category from './category'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    // firebase user data
    auth: auth,
    // custom user data for renty
    user: user,
    shared: shared,
    item: item,
    category: category
  }
})

export default store
