import db from '../../db'

export default {
  state: {
    categories: []
  },
  mutations: {
    setCategories (state, payload) {
      state.categories = payload
    }
  },
  actions: {
    fetchListOfCategories ({ commit, getters }) {
      commit('setLoading', { action: 'Fetching Categories' })
      db.collection('categories')
        .get()
        .then(data => {
          const categories = []
          data.forEach(function (category) {
            const categoryData = category.data()
            categoryData.id = category.id
            categories.push(categoryData)
          })
          commit('setCategories', categories)
          commit('setLoading', false)
        })
        .catch(e => {
          commit('setError', e)
          commit('setLoading', false)
        })
    }
  },
  getters: {
    categories (state) {
      return state.categories
    }
  }
}
