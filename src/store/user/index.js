import firebase from 'firebase/app'
import 'firebase/auth'
import db from '../../db'
import router from '../../router'

export default {
  state: {
    user: null
  },
  mutations: {
    updateUser (state, payload) {
      state.User = payload
    },
    setUser (state, payload) {
      state.user = payload
    }
  },
  actions: {
    fetchUserData ({ commit, getters }, id) {
      commit('setLoading', { action: 'Fetching User Data' })

      db.doc('users/' + id)
        .get()
        .then(data => {
          if (!data.exists) {
            commit('setUser', null)
            commit('setLoading', false)
            router.push('/me/profile/update')
          } else {
            const userData = data.data()
            commit('setUser', userData)
            commit('setLoading', false)
          }
        })
        .catch(e => {
          commit('setError', e)
          commit('setLoading', false)
        })
    },
    updateUser ({ commit, getters }, payload) {
      commit('setLoading', { action: 'Updating User Data' })

      let imageUrl
      const userId = getters.auth.uid
      const userData = {
        firstName: payload.firstName,
        lastName: payload.lastName,
        address: payload.address
      }

      db.collection('users').doc(userId).set(userData)
        .then((data) => {
          return userId
        })
        .then(function () {
          if (payload.image) {
            const filename = payload.image.name
            const ext = filename.slice(filename.lastIndexOf('.'))
            return firebase.storage().ref('images/user/' + userId + '.' + ext).put(payload.image)
          } else {
            return null
          }
        })
        .then(fileData => {
          if (fileData) {
            imageUrl = fileData.metadata.downloadURLs[0]
          } else {
            if (payload.imageUrl) {
              imageUrl = payload.imageUrl
            } else {
              imageUrl = 'https://pngimg.com/uploads/minions/minions_PNG24.png'
            }
          }
          return db.doc('users/' + userId).update({ imageUrl: imageUrl })
        })
        .then(() => {
          userData.imageUrl = imageUrl
          commit('setUser', userData)
          commit('setLoading', false)
          router.replace('/me/profile')
        })
        .catch(e => {
          commit('setError', e)
          commit('setLoading', false)
        })
    }
  },
  getters: {
    user (state) {
      return state.user
    }
  }
}
