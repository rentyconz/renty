/* Get firebase */

const functions = require('firebase-functions');
const admin = require('firebase-admin');

/* Initialize db. We may need it later */
//admin.initializeApp(functions.config().firebase);

exports.renderViewDev = functions.https.onRequest((request, response) => {

    var reqHost = request.host;
    var reqSub = request.subdomains;
    var reqPath = request.path;

    /* Log req */
    //admin.database().ref('/requesturl').push({original: original}).then(snapshot => {
    //});

    var requestor = require('request');
    var cheerio = require('cheerio');

    /* Temporary, hard set for testing */
    var getUrl = 'https://develop.renty.co.nz';
    var metaTitle = 'This is a render test';

    requestor(getUrl, function (error, res, html) {
        if (!error && res.statusCode == 200) {
            var $ = cheerio.load(html);
            $('title').text(metaTitle+' reqSub '+reqSub);
            $('<base href="https://develop.renty.co.nz">').appendTo('head');
            var render=$.html();

            /* Return HTML */
            response.send(render);
        }
    });

});

exports.renderView = functions.https.onRequest((request, response) => {

    var reqHost = request.host;
    var reqSub = request.subdomains;
    var reqPath = request.path;

    /* Log req */
    //admin.database().ref('/requesturl').push({original: original}).then(snapshot => {
    //});

    var requestor = require('request');
    var cheerio = require('cheerio');

    /* Temporary, hard set for testing */
    var getUrl = 'https://www.renty.co.nz';
    var metaTitle = 'This is a render test';

    requestor(getUrl, function (error, res, html) {
        if (!error && res.statusCode == 200) {
            var $ = cheerio.load(html);
            $('title').text(metaTitle+' reqSub '+reqSub);
            $('<base href="https://www.renty.co.nz">').appendTo('head');
            var render=$.html();

            /* Return HTML */
            response.send(render);
        }
    });

});